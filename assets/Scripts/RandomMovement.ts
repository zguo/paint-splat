
import { _decorator, Component, Node, Vec3, UITransformComponent } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = RandomMovement
 * DateTime = Thu Oct 21 2021 17:03:38 GMT+0100 (爱尔兰标准时间)
 * Author = Fonshron
 * FileBasename = RandomMovement.ts
 * FileBasenameNoExtension = RandomMovement
 * URL = db://assets/Scripts/RandomMovement.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass('RandomMovement')
export class RandomMovement extends Component {
    // [1]
    // dummy = '';
    @property
    FRAME_GAP: number = 5;

    framePassed: number = 0;
    sceneWidth: number = 0;
    sceneHeight: number = 0;
    width: number = 0;
    height: number = 0;
    yUpperLimit: number = 0;
    yLowerLimit: number = 0;
    xUpperLimit: number = 0;
    xLowerLimit: number = 0;

    start() {
        var uiComponent = this.node.parent.getComponent(UITransformComponent);
        this.sceneWidth = uiComponent.contentSize.width;
        this.sceneHeight = uiComponent.contentSize.height;
        uiComponent = this.node.getComponent(UITransformComponent);
        this.width = uiComponent.width;
        this.height = uiComponent.height;
        this.calBoundaryLimit();
    }

    update(deltaTime: number) {
        if (this.framePassed >= this.FRAME_GAP) {
            var position: Vec3 = new Vec3(
                (this.xUpperLimit - this.xLowerLimit) * Math.random() + this.xLowerLimit,
                (this.yUpperLimit - this.yLowerLimit) * Math.random() + this.yLowerLimit,
                0);
            this.node.setPosition(position);
            this.framePassed = 0;
        } else {
            this.framePassed++;
        }
    }

    calBoundaryLimit() {
        this.yUpperLimit = (this.sceneHeight - this.height) / 2;
        this.yLowerLimit = -1 * this.yUpperLimit;
        this.xUpperLimit = (this.sceneWidth - this.width) / 2;
        this.xLowerLimit = -1 * this.xUpperLimit;
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.3/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.3/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.3/manual/en/scripting/life-cycle-callbacks.html
 */
